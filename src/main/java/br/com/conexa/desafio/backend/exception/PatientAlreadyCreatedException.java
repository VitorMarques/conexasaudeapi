package br.com.conexa.desafio.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class PatientAlreadyCreatedException extends RuntimeException {

    public PatientAlreadyCreatedException(String message) {
        super(message);
    }

}
