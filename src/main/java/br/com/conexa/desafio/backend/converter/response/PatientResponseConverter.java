package br.com.conexa.desafio.backend.converter.response;

import br.com.conexa.desafio.backend.dto.response.PatientResponse;
import br.com.conexa.desafio.backend.model.Patient;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class PatientResponseConverter implements Function<Patient, PatientResponse> {

    @Override
    public PatientResponse apply(Patient patient) {
        PatientResponse patientResponse = new PatientResponse();
        patientResponse.setUuid(patient.getUuid());
        patientResponse.setName(patient.getName());
        patientResponse.setAge(patient.getAge());
        patientResponse.setCpf(patient.getCpf());
        patientResponse.setPhone(patient.getPhone());
        return patientResponse;
    }

    public List<PatientResponse> convertToList(List<Patient> patients) {
        return patients.stream().map(this::apply).collect(Collectors.toList());
    }

}
