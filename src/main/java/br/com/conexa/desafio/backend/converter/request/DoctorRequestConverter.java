package br.com.conexa.desafio.backend.converter.request;

import br.com.conexa.desafio.backend.dto.request.DoctorRequest;
import br.com.conexa.desafio.backend.model.Doctor;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.function.Function;

@RequiredArgsConstructor
@Component
public class DoctorRequestConverter implements Function<DoctorRequest, Doctor> {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Doctor apply(DoctorRequest doctorRequest) {
        Doctor doctor = new Doctor();
        doctor.setUuid(UUID.randomUUID().toString());
        doctor.setName(doctorRequest.getName());
        doctor.setUsername(doctorRequest.getUsername());
        doctor.setPassword(bCryptPasswordEncoder.encode(doctorRequest.getPassword()));
        return doctor;
    }

}
