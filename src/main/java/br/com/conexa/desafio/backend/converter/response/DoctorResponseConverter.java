package br.com.conexa.desafio.backend.converter.response;

import br.com.conexa.desafio.backend.dto.response.DoctorResponse;
import br.com.conexa.desafio.backend.model.Doctor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class DoctorResponseConverter implements Function<Doctor, DoctorResponse> {

    @Override
    public DoctorResponse apply(Doctor doctor) {
        DoctorResponse doctorResponse = new DoctorResponse();
        doctorResponse.setUuid(doctor.getUuid());
        doctorResponse.setName(doctor.getName());
        doctorResponse.setUsername(doctor.getUsername());
        if(doctor.getSpecialty() != null) {
            doctorResponse.setSpecialtyUuid(doctor.getSpecialty().getUuid());
            doctorResponse.setSpecialtyDescription(doctor.getSpecialty().getDescription());
        }
        return doctorResponse;
    }

    public List<DoctorResponse> convertToList(List<Doctor> doctors) {
        return doctors.stream().map(this::apply).collect(Collectors.toList());
    }

}
