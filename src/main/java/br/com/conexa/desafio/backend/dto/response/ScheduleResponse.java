package br.com.conexa.desafio.backend.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ScheduleResponse {

    @JsonProperty(value = "patient_uuid")
    private String patientUuid;

    @JsonProperty(value = "service_date_time")
    private LocalDateTime serviceDateAndTime;

}
