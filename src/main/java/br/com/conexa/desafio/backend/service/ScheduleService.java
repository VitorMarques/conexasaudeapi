package br.com.conexa.desafio.backend.service;

import br.com.conexa.desafio.backend.model.Schedule;
import br.com.conexa.desafio.backend.repository.ScheduleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

@Service
@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ScheduleService {

    private final ScheduleRepository scheduleRepository;

    @Cacheable("schedules")
    public Page<Schedule> findAllSchedulesByDoctor(Long doctorId, Pageable pageable) {

        log.info("Retrieving all schedules for the doctor " + doctorId);

        return nonNullSchedules(scheduleRepository.findAllByDoctorId(doctorId, pageable));
    }

    @Cacheable("day-schedules")
    public Page<Schedule> findDaySchedulesByDoctor(Long doctorId, Pageable pageable) {

        log.info("Retrieving all day schedules for the doctor " + doctorId);

        return nonNullSchedules(scheduleRepository.findDaySchedulesByDoctor(doctorId, pageable));
    }

    @Transactional(readOnly = false)
    public Schedule save(Schedule schedule) {

        log.info("Scheduling appointment for patient "
                + schedule.getPatient().getName() + " with doctor "
                + schedule.getDoctor().getName() + " on "
                + schedule.getServiceDateAndTime().toLocalDate() + " at "
                + schedule.getServiceDateAndTime().toLocalTime()
        );

        return scheduleRepository.save(schedule);
    }

    private Page<Schedule> nonNullSchedules(Page<Schedule> schedules) {
        return CollectionUtils.isEmpty(schedules.getContent()) ? Page.empty() : schedules;
    }

}
