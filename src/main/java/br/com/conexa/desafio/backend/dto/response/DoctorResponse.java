package br.com.conexa.desafio.backend.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoctorResponse {

    private String uuid;
    private String name;
    private String username;
    private String specialtyUuid;
    private String specialtyDescription;
    private Set<ScheduleResponse> schedules;

}
