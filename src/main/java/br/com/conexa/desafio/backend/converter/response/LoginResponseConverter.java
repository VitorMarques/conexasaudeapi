package br.com.conexa.desafio.backend.converter.response;

import br.com.conexa.desafio.backend.dto.response.LoginResponse;
import br.com.conexa.desafio.backend.model.Doctor;
import br.com.conexa.desafio.backend.model.Specialty;
import br.com.conexa.desafio.backend.service.ScheduleService;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class LoginResponseConverter implements Function<Doctor, LoginResponse> {

    private final ScheduleService scheduleService;
    private final ScheduleResponseConverter scheduleResponseConverter;

    @Override
    public LoginResponse apply(Doctor doctor) {
        LoginResponse loginResponse = new LoginResponse();

        loginResponse.setName(doctor.getName());

        loginResponse.setDoctorUuid(doctor.getUuid());

        Specialty specialty = doctor.getSpecialty();

        if(specialty != null && !StringUtils.isEmpty(specialty.getDescription()))
            loginResponse.setSpecialtyDescription(specialty.getDescription());

        loginResponse.setSchedules(scheduleResponseConverter.convertToSet(
                scheduleService.findDaySchedulesByDoctor(doctor.getId(), Pageable.unpaged())
                        .getContent()
                        .stream()
                        .collect(Collectors.toSet())
                )
        );

        return loginResponse;
    }

}
