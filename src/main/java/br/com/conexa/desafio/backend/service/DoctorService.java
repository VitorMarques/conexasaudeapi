package br.com.conexa.desafio.backend.service;

import br.com.conexa.desafio.backend.exception.NotFoundException;
import br.com.conexa.desafio.backend.model.Doctor;
import br.com.conexa.desafio.backend.repository.DoctorRepository;
import br.com.conexa.desafio.backend.util.MessageUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class DoctorService {

    private final MessageUtil messageUtil;
    private final DoctorRepository doctorRepository;

    @Transactional(readOnly = false)
    public Doctor save(Doctor doctor) {
        return doctorRepository.save(doctor);
    }

    public Page<Doctor> findAll(Pageable pageable) {
        return doctorRepository.findAll(pageable);
    }

    public Doctor findByUuid(String uuid) throws NotFoundException {
        return doctorRepository.findByUuid(uuid)
                .orElseThrow(() -> new NotFoundException(messageUtil.getMessage("error.doctor.uuid.not.found", uuid)));
    }

}
