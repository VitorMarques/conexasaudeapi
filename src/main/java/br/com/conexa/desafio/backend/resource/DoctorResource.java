package br.com.conexa.desafio.backend.resource;

import br.com.conexa.desafio.backend.converter.request.DoctorRequestConverter;
import br.com.conexa.desafio.backend.converter.response.DoctorResponseConverter;
import br.com.conexa.desafio.backend.converter.request.ScheduleRequestConverter;
import br.com.conexa.desafio.backend.converter.response.ScheduleResponseConverter;
import br.com.conexa.desafio.backend.dto.request.DoctorRequest;
import br.com.conexa.desafio.backend.dto.request.ScheduleRequest;
import br.com.conexa.desafio.backend.dto.response.DoctorResponse;
import br.com.conexa.desafio.backend.model.Doctor;
import br.com.conexa.desafio.backend.model.Schedule;
import br.com.conexa.desafio.backend.service.DoctorService;
import br.com.conexa.desafio.backend.service.ScheduleService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/doctors")
public class DoctorResource extends Resource {

    private final DoctorService doctorService;
    private final ScheduleService scheduleService;
    private final DoctorRequestConverter doctorRequestConverter;
    private final DoctorResponseConverter doctorResponseConverter;
    private final ScheduleRequestConverter scheduleRequestConverter;
    private final ScheduleResponseConverter scheduleResponseConverter;

    @PostMapping
    public ResponseEntity<DoctorResponse> save(@RequestBody @Valid DoctorRequest doctorRequest) {

        return created(doctorService.save(doctorRequestConverter.apply(doctorRequest)), doctorResponseConverter);
    }

    @PostMapping(value = "/{uuid}/schedules", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DoctorResponse> scheduleAppointment(@PathVariable String uuid,
                                                 @RequestBody @Valid ScheduleRequest scheduleRequest) {

        scheduleRequest.setDoctorUuid(uuid);

        return created(scheduleService.save(scheduleRequestConverter.apply(scheduleRequest)), scheduleResponseConverter);
    }

    @GetMapping(value = "/{uuid}/schedules")
    public ResponseEntity<DoctorResponse> getAllSchedules(@PathVariable String uuid,
                                             @PageableDefault Pageable pageable,
                                             @RequestParam(name = "today", defaultValue = "false", required = false) Boolean today) {

        Doctor doctor = doctorService.findByUuid(uuid);

        Page<Schedule> doctorSchedules =
                today ? scheduleService.findDaySchedulesByDoctor(doctor.getId(), pageable)
                        : scheduleService.findAllSchedulesByDoctor(doctor.getId(), pageable);

        return ok(doctorSchedules, scheduleResponseConverter);
    }

}
