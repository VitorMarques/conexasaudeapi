package br.com.conexa.desafio.backend.dto.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class DoctorRequest {

    @NotNull(message = "{doctor.name.not.null}")
    @NotEmpty(message = "{doctor.name.not.empty}")
    private String name;

    @NotNull(message = "{doctor.username.not.null}")
    @NotEmpty(message = "{doctor.username.not.empty}")
    private String username;

    @NotNull(message = "{doctor.password.not.null}")
    @NotEmpty(message = "{doctor.password.not.empty}")
    private String password;

}
