package br.com.conexa.desafio.backend.service;

import br.com.conexa.desafio.backend.exception.TokenNotFoundException;
import br.com.conexa.desafio.backend.model.Token;
import br.com.conexa.desafio.backend.repository.TokenRepository;
import br.com.conexa.desafio.backend.security.JwtConfiguration;
import br.com.conexa.desafio.backend.util.MessageUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.sql.Date;
import java.time.LocalDate;

@Service
@Slf4j
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class TokenService {

    private final MessageUtil messageUtil;
    private final TokenRepository tokenRepository;
    private final JwtConfiguration jwtConfiguration;

    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public String generateToken(String username) throws UsernameNotFoundException {

        log.info("Trying generate token for user " + username);

        if(StringUtils.isEmpty(username))
            throw new UsernameNotFoundException(messageUtil.getMessage("token.security.username.not.provided"));

        Date expirationDate = Date.valueOf(LocalDate.now().plusDays(jwtConfiguration.getTimeToLive()));

        String token = JWT.create()
                .withSubject(username)
                .withIssuedAt(new java.util.Date())
                .withExpiresAt(expirationDate)
                .sign(Algorithm.HMAC512(jwtConfiguration.getSecretKey().getBytes()));

        if(!StringUtils.isEmpty(token))
            save(token, username, expirationDate);

        log.info("Token generated with success -> " + token);

        return token;

    }

    public void save(String token, String username, Date expirationDate) {

        log.info("Saving token " + token + " on database. Token valid until " + expirationDate);

        tokenRepository.save(
                Token.builder()
                        .token(token)
                        .username(username)
                        .expirationDate(LocalDate.from(expirationDate.toLocalDate()))
                        .valid(Boolean.TRUE)
                        .build()
        );
    }

    public Boolean isValid(String token) {

        Token databaseToken = getDatabaseToken(token);

        if(token.equalsIgnoreCase(databaseToken.getToken()))
            return databaseToken.isValid() && databaseToken.getExpirationDate().isAfter(LocalDate.now());

        return false;

    }

    @Transactional(readOnly = false)
    public void invalidate(String token) {

        Token databaseToken = getDatabaseToken(token);

        databaseToken.setValid(Boolean.FALSE);
        databaseToken.setExpirationDate(LocalDate.now());

        tokenRepository.save(databaseToken);

        log.info("Token " + token + " invalidated on " + LocalDate.now());

    }
    public String extractUsernameFrom(String token) {
        return JWT.require(Algorithm.HMAC512(jwtConfiguration.getSecretKey())).build().verify(token).getSubject();
    }

    private Token getDatabaseToken(String token) throws TokenNotFoundException {
        String username = extractUsernameFrom(token);
        return tokenRepository
                .findByUsername(username)
                .orElseThrow(() -> new TokenNotFoundException(messageUtil.getMessage("token.security.username.not.found")));
    }

}
