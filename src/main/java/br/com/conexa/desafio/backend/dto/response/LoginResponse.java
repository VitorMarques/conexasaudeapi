package br.com.conexa.desafio.backend.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Set;

@Data
@ToString(doNotUseGetters = true)
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponse {

    private String token;

    private String name;

    @JsonProperty(value = "doctor_uuid")
    private String doctorUuid;

    @JsonProperty(value = "specialty")
    private String specialtyDescription;

    @JsonProperty(value = "today_schedules")
    private Set<ScheduleResponse> schedules;

}
