package br.com.conexa.desafio.backend.converter.request;

import br.com.conexa.desafio.backend.dto.request.PatientRequest;
import br.com.conexa.desafio.backend.model.Patient;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.function.Function;

@Component
public class PatientRequestConverter implements Function<PatientRequest, Patient> {

    @Override
    public Patient apply(PatientRequest patientRequest) {
        Patient patient = new Patient();

        if(patientRequest.getUuid() == null || patientRequest.getUuid().isEmpty())
            patient.setUuid(UUID.randomUUID().toString());
        else
            patient.setUuid(patientRequest.getUuid());

        patient.setName(patientRequest.getName());
        patient.setAge(patientRequest.getAge());
        patient.setCpf(patientRequest.getCpf());
        patient.setPhone(patientRequest.getPhone());
        return patient;
    }

}
