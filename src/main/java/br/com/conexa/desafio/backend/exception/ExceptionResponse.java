package br.com.conexa.desafio.backend.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class ExceptionResponse {

    private Date timestamp;
    private String message;
    private String[] details;

    public ExceptionResponse(Date timestamp, String message, String... details) {
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
    }

}
