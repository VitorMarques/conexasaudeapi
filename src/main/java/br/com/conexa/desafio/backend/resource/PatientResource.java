package br.com.conexa.desafio.backend.resource;

import br.com.conexa.desafio.backend.converter.request.PatientRequestConverter;
import br.com.conexa.desafio.backend.converter.response.PatientResponseConverter;
import br.com.conexa.desafio.backend.dto.request.PatientRequest;
import br.com.conexa.desafio.backend.dto.response.PatientResponse;
import br.com.conexa.desafio.backend.service.PatientService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@RestController
@RequestMapping("/patients")
public class PatientResource extends Resource {

    private final PatientService patientService;
    private final PatientRequestConverter requestConverter;
    private final PatientResponseConverter responseConverter;

    @GetMapping
    public ResponseEntity<PatientResponse> getAll(@PageableDefault Pageable pageable) {

        return ok(patientService.findAll(pageable), responseConverter);
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<PatientResponse> getByUuid(@PathVariable String uuid) {

        return ok(patientService.findByUuid(uuid), responseConverter);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PatientResponse> save(@RequestBody @Valid PatientRequest patientRequest) {

        return created(patientService.save(requestConverter.apply(patientRequest)), responseConverter);
    }

    @PutMapping("/{uuid}")
    public ResponseEntity<PatientResponse> update(@PathVariable @NotNull String uuid,
                                                  @RequestBody @Valid PatientRequest patientRequest) {

        patientRequest.setUuid(uuid);
        return ok(patientService.update(requestConverter.apply(patientRequest)), responseConverter);
    }

    @DeleteMapping("/{uuid}")
    public ResponseEntity<?> delete(@PathVariable String uuid) {

        patientService.delete(uuid);
        return noContent();
    }

}
