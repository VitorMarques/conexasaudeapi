package br.com.conexa.desafio.backend.security;

import br.com.conexa.desafio.backend.service.TokenService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private final JwtConfiguration jwtConfiguration;
    private final TokenService tokenService;

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager,
                                  JwtConfiguration jwtConfiguration,
                                  TokenService tokenService) {
        super(authenticationManager);
        this.jwtConfiguration = jwtConfiguration;
        this.tokenService = tokenService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {

        String header = request.getHeader(jwtConfiguration.getHeaderName());

        if (header == null || !header.startsWith(jwtConfiguration.getHeaderPrefix())) {
            chain.doFilter(request, response);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(request);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {

        String token = request.getHeader(jwtConfiguration.getHeaderName()).replace(jwtConfiguration.getHeaderPrefix(), "");

        if (tokenService.isValid(token))
            return new UsernamePasswordAuthenticationToken(tokenService.extractUsernameFrom(token), null, new ArrayList<>());

        return null;
    }
}
