package br.com.conexa.desafio.backend.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class ScheduleRequest {

    @NotNull(message = "{schedule.date.time.not.null}")
    @Future(message = "{schedule.date.time.not.empty}")
    @JsonProperty(value = "service_date_time")
    private LocalDateTime serviceDateAndTime;

    @NotNull(message = "{schedule.patient.uuid.not.null}")
    @JsonProperty(value = "patient_uuid")
    private String patientUuid;

    private String doctorUuid;

}
