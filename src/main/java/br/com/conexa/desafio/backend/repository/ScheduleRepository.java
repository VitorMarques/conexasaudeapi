package br.com.conexa.desafio.backend.repository;

import br.com.conexa.desafio.backend.model.Schedule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ScheduleRepository extends PagingAndSortingRepository<Schedule, Long> {

    @Query(value =
            "SELECT * FROM schedule WHERE doctor_id = :doctorId AND DATE(service_date_time) = DATE(NOW())",
            nativeQuery = true)
    Page<Schedule> findDaySchedulesByDoctor(@Param("doctorId") Long doctorId, Pageable pageable);

    @Query(value =
            "SELECT * FROM schedule WHERE doctor_id = :doctorId", nativeQuery = true)
    Page<Schedule> findAllByDoctorId(@Param("doctorId") Long doctorId, Pageable pageable);

}
