package br.com.conexa.desafio.backend.repository;

import br.com.conexa.desafio.backend.model.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthenticationRepository extends JpaRepository<Doctor, Long> {

    Optional<Doctor> findByUsername(String username);

}
