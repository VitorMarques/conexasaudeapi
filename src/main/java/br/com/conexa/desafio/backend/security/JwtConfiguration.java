package br.com.conexa.desafio.backend.security;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Data
@NoArgsConstructor
@Component
@ConfigurationProperties(prefix = "jwt.security")
public class JwtConfiguration {

    private String secretKey;
    private String headerName;
    private String headerPrefix;
    private Integer timeToLive;
    private String[] publicPostRoutes;
    private String[] publicGetRoutes;

    public String getAuthorizationHeader() {
        return HttpHeaders.AUTHORIZATION;
    }
}
