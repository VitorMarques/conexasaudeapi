package br.com.conexa.desafio.backend.converter.response;

import br.com.conexa.desafio.backend.dto.response.ScheduleResponse;
import br.com.conexa.desafio.backend.model.Patient;
import br.com.conexa.desafio.backend.model.Schedule;
import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class ScheduleResponseConverter implements Function<Schedule, ScheduleResponse> {

    @Override
    public ScheduleResponse apply(Schedule schedule) {
        ScheduleResponse scheduleResponse = new ScheduleResponse();

        Patient patient = schedule.getPatient();

        if(patient != null && !StringUtils.isEmpty(patient.getUuid()))
            scheduleResponse.setPatientUuid(patient.getUuid());

        scheduleResponse.setServiceDateAndTime(schedule.getServiceDateAndTime());
        return scheduleResponse;
    }

    public Set<ScheduleResponse> convertToSet(Set<Schedule> schedules) {
        return schedules.stream().map(this::apply).collect(Collectors.toSet());
    }

}
