package br.com.conexa.desafio.backend.converter.request;

import br.com.conexa.desafio.backend.dto.request.ScheduleRequest;
import br.com.conexa.desafio.backend.model.Schedule;
import br.com.conexa.desafio.backend.service.DoctorService;
import br.com.conexa.desafio.backend.service.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.function.Function;

@Component
@RequiredArgsConstructor
public class ScheduleRequestConverter implements Function<ScheduleRequest, Schedule> {

    private final PatientService patientService;
    private final DoctorService doctorService;

    @Override
    public Schedule apply(ScheduleRequest scheduleRequest) {
        Schedule schedule = new Schedule();
        schedule.setUuid(UUID.randomUUID().toString());
        schedule.setServiceDateAndTime(scheduleRequest.getServiceDateAndTime());
        schedule.setPatient(patientService.findByUuid(scheduleRequest.getPatientUuid()));
        schedule.setDoctor(doctorService.findByUuid(scheduleRequest.getDoctorUuid()));
        return schedule;
    }

}
