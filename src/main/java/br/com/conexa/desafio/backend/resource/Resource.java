package br.com.conexa.desafio.backend.resource;

import br.com.conexa.desafio.backend.model.Model;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class Resource {

    private static final String UUID = "/{uuid}";

    protected ResponseEntity noContent() {
        return ResponseEntity.noContent().build();
    }

    protected <T extends Object> ResponseEntity ok(@NonNull T resource, @NonNull Function converter) {
        if(resource instanceof Page)
            return ResponseEntity.ok(((Page) resource).stream().map(converter).collect(Collectors.toSet()));
        return ResponseEntity.ok(converter.apply(resource));
    }

    protected <T extends Model> ResponseEntity created(@NonNull T resource, @NonNull Function converter) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .location(getResourceLocation(resource))
                .body(converter.apply(resource));
    }

    protected <T extends Model> URI getResourceLocation(@NonNull T resource) {
        return ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path(UUID)
                .buildAndExpand(resource.getUuid()).toUri();
    }

}
