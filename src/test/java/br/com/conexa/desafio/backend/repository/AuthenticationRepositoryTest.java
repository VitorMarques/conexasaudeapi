package br.com.conexa.desafio.backend.repository;

import br.com.conexa.desafio.backend.model.Doctor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class AuthenticationRepositoryTest {

    @Autowired
    private AuthenticationRepository repository;

    public static final String DOCTOR_USERNAME = "medico@email.com";

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void shouldFindByUsername() {
        Doctor doctor = repository.findByUsername(DOCTOR_USERNAME).get();
        assertAll(
                () -> assertNotNull(doctor),
                () -> assertTrue(doctor.getId() > 0),
                () -> assertTrue(DOCTOR_USERNAME.equalsIgnoreCase(doctor.getUsername())),
                () -> assertTrue(StringUtils.isNotBlank(doctor.getName())),
                () -> assertTrue(StringUtils.isNotBlank(doctor.getPassword())),
                () -> assertTrue(StringUtils.isNotBlank(doctor.getUuid())),
                () -> assertTrue(doctor.getSpecialty() != null && doctor.getSpecialty().getId() > 0)
        );
    }
}