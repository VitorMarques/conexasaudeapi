package br.com.conexa.desafio.backend.resource;

import br.com.conexa.desafio.backend.dto.request.DoctorRequest;
import br.com.conexa.desafio.backend.dto.request.ScheduleRequest;
import br.com.conexa.desafio.backend.service.DoctorService;
import br.com.conexa.desafio.backend.service.PatientService;
import br.com.conexa.desafio.backend.service.TokenService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DoctorResourceTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private PatientService patientService;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void shouldSave() throws Exception {

        DoctorRequest doctorRequest = new DoctorRequest();
        doctorRequest.setName("dr. Vitor Marques Teste 2");
        doctorRequest.setUsername("usernamedomedico@email.com");
        doctorRequest.setPassword(new BCryptPasswordEncoder().encode("senhadomediconovo"));

        mockMvc.perform(
                post("/doctors")
                        .header(
                                HttpHeaders.AUTHORIZATION,
                                "Bearer " + tokenService.generateToken("usernamedomedico@email.com" + Math.random()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(doctorRequest))
        ).andExpect(
                status().isCreated())
        .andExpect(
                header().exists(HttpHeaders.LOCATION)
        );

    }

    @Test
    void shouldScheduleAppointment() throws Exception {

        String doctorUuid = doctorService.findAll(Pageable.unpaged()).stream().findFirst().get().getUuid();
        String patientUuid = patientService.findAll(Pageable.unpaged()).stream().findFirst().get().getUuid();

        ScheduleRequest scheduleRequest = new ScheduleRequest();
        scheduleRequest.setServiceDateAndTime(LocalDateTime.now().plusDays(7));
        scheduleRequest.setDoctorUuid(doctorUuid);
        scheduleRequest.setPatientUuid(patientUuid);

        mockMvc.perform(
                post("/doctors/"+doctorUuid+"/schedules")
                        .header(
                                HttpHeaders.AUTHORIZATION,
                                "Bearer " + tokenService.generateToken("usernamedomedico@email.com" + Math.random()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(scheduleRequest))
        ).andExpect(
                status().isCreated())
        .andExpect(
                header().exists(HttpHeaders.LOCATION)
        );

    }

    @Test
    void shouldGetAllSchedules() throws Exception {

        String doctorUuid = doctorService.findAll(Pageable.unpaged()).stream().findFirst().get().getUuid();

        mockMvc.perform(
                get("/doctors/"+doctorUuid+"/schedules")
                        .header(
                                HttpHeaders.AUTHORIZATION,
                                "Bearer " + tokenService.generateToken("usernamedomedico@email.com" + Math.random()))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());

    }

    @Test
    void shouldGetDaySchedules() throws Exception {

        String doctorUuid = doctorService.findAll(Pageable.unpaged()).stream().findFirst().get().getUuid();

        mockMvc.perform(
                get("/doctors/"+doctorUuid+"/schedules?today=true")
                        .header(
                                HttpHeaders.AUTHORIZATION,
                                "Bearer " + tokenService.generateToken("usernamedomedico@email.com" + Math.random()))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());

    }

}