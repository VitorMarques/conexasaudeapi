package br.com.conexa.desafio.backend.repository;

import br.com.conexa.desafio.backend.model.Doctor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DoctorRepositoryTest {

    @Autowired
    private DoctorRepository repository;

    @Autowired
    private SpecialtyRepository specialtyRepository;

    public static final String DOCTOR_NAME = "dr. Vitor Marques";
    private static final String DOCTOR_UUID = UUID.randomUUID().toString();
    private static final String DOCTOR_USERNAME = "medicovitor@email.com";
    private static final String DOCTOR_PASSWORD = new BCryptPasswordEncoder().encode("senhadomedicovitor");

    @BeforeEach
    void setUp() {
        repository.save(Doctor.builder()
                .name(DOCTOR_NAME)
                .username(DOCTOR_USERNAME)
                .password(DOCTOR_PASSWORD)
                .specialty(specialtyRepository.findById(1L).get())
                .uuid(DOCTOR_UUID)
                .build());
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void shouldFindByUuid() {
        Doctor doctor = repository.findByUuid(DOCTOR_UUID).get();
        assertAll(
                () -> assertNotNull(doctor),
                () -> assertTrue(doctor.getId() > 0),
                () -> assertTrue(DOCTOR_UUID.equalsIgnoreCase(doctor.getUuid())),
                () -> assertTrue(StringUtils.isNotBlank(doctor.getName())),
                () -> assertEquals(DOCTOR_USERNAME, doctor.getUsername()),
                () -> assertEquals(DOCTOR_PASSWORD, doctor.getPassword()),
                () -> assertNotNull(doctor.getSpecialty())
        );
    }
}