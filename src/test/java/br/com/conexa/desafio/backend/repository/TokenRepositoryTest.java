package br.com.conexa.desafio.backend.repository;

import br.com.conexa.desafio.backend.model.Token;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Date;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TokenRepositoryTest {

    @Autowired
    private TokenRepository repository;

    private static final String DOCTOR_USERNAME = "medicovitor@email.com";
    private static final LocalDate EXPIRATION_DATE = LocalDate.now().plusDays(7);
    private static final String TEST_SECRETKEY = "testsecretkey";

    @BeforeEach
    void setUp() {

        String token = JWT.create()
                .withSubject(DOCTOR_USERNAME)
                .withIssuedAt(new java.util.Date())
                .withExpiresAt(Date.valueOf(EXPIRATION_DATE))
                .sign(Algorithm.HMAC512(TEST_SECRETKEY));

        repository.save(Token.builder()
                .username(DOCTOR_USERNAME)
                .valid(Boolean.TRUE)
                .expirationDate(EXPIRATION_DATE)
                .token(token)
                .build());
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void shouldFindByUsername() {
        Token token = repository.findByUsername(DOCTOR_USERNAME).get();

        assertAll(
                () -> assertNotNull(token),
                () -> assertTrue(token.getId() > 0),
                () -> assertEquals(DOCTOR_USERNAME, token.getUsername()),
                () -> assertNotNull(token.getExpirationDate()),
                () -> assertNotNull(token.getToken())
        );

    }
}