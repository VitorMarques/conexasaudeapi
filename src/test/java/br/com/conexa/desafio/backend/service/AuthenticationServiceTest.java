package br.com.conexa.desafio.backend.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class AuthenticationServiceTest {

    @Autowired
    private AuthenticationService service;

    public static final String USERNAME = "medico@email.com";
    public static final String INVALID_NOT_FOUND = "usernameNotFound";

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void shouldLoadUserByUsername() {
        assertTrue(StringUtils.isNotBlank(service.loadUserByUsername(USERNAME).getUsername()));
    }

    @Test
    void shouldThrowNotFoundException() {
        assertThrows(UsernameNotFoundException.class,
                () -> service.loadUserByUsername(INVALID_NOT_FOUND));
    }
}