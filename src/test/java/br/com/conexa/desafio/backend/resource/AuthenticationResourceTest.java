package br.com.conexa.desafio.backend.resource;

import br.com.conexa.desafio.backend.dto.request.DoctorRequest;
import br.com.conexa.desafio.backend.dto.request.LoginRequest;
import br.com.conexa.desafio.backend.service.TokenService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.UUID;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AuthenticationResourceTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TokenService tokenService;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void shouldLogin() throws Exception {

        LoginRequest loginRequest = new LoginRequest("medico@email.com", "senhadomedico");

        mockMvc.perform(
                post("/authentication/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequest))
            ).andExpect(status().isOk());

    }

    @Test
    void shouldReturnBadCredentials() throws Exception {

        LoginRequest loginRequest = new LoginRequest("medico_invalido@email.com", "senhadomedicoinvalido");
        mockMvc.perform(
                post("/authentication/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(loginRequest))
        ).andExpect(status().isUnauthorized());

    }

    @Test
    void shouldLogout() throws Exception {
        mockMvc.perform(
                get("/authentication/logout")
                .header("Authorization", "Bearer " + tokenService.generateToken("medico_invalido@email.com"))
        ).andExpect(status().isNoContent());
    }
}