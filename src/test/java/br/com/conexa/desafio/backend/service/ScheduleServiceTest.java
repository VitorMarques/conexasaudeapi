package br.com.conexa.desafio.backend.service;

import br.com.conexa.desafio.backend.model.Doctor;
import br.com.conexa.desafio.backend.model.Patient;
import br.com.conexa.desafio.backend.model.Schedule;
import br.com.conexa.desafio.backend.repository.DoctorRepository;
import br.com.conexa.desafio.backend.repository.PatientRepository;
import br.com.conexa.desafio.backend.repository.SpecialtyRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ScheduleServiceTest {

    @Autowired
    private ScheduleService service;

    @Autowired
    private SpecialtyRepository specialtyRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private PatientRepository patientRepository;

    public static final String DOCTOR_NAME = "dr. Vitor Marques";
    private static final String DOCTOR_USERNAME = "medicovitor@email.com";
    private static final String DOCTOR_PASSWORD = new BCryptPasswordEncoder().encode("senhadomedicovitor");

    private Doctor doctor;
    private Schedule schedule;
    private Patient patient;

    @BeforeEach
    public void setUp() {
        this.doctor = doctorRepository.save(Doctor.builder()
                .name(DOCTOR_NAME + Math.random())
                .username(DOCTOR_USERNAME + Math.random())
                .password(DOCTOR_PASSWORD + Math.random())
                .specialty(specialtyRepository.findById(1L).get())
                .uuid(UUID.randomUUID().toString())
                .build());

        this.patient = patientRepository.save(Patient.builder()
                .name("Vitor Marques" + Math.random())
                .age(32)
                .cpf("111.328.227.70")
                .phone("21 969236537")
                .uuid(UUID.randomUUID().toString())
                .build());

        this.schedule = new Schedule();
        this.schedule.setServiceDateAndTime(LocalDateTime.now().plusDays(7));
        this.schedule.setDoctor(this.doctor);
        this.schedule.setPatient(this.patient);
        this.schedule.setUuid(UUID.randomUUID().toString());

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void shouldFindAllSchedulesByDoctor() {
        assertDoesNotThrow(() -> service.findAllSchedulesByDoctor(this.doctor.getId(), Pageable.unpaged()));
    }

    @Test
    void shouldFindDaySchedulesByDoctor() {
        assertDoesNotThrow(() -> service.findDaySchedulesByDoctor(this.doctor.getId(), Pageable.unpaged()));
    }

    @Test
    @Order(1)
    void shouldSave() {
        assertNotNull(service.save(this.schedule));
    }
}