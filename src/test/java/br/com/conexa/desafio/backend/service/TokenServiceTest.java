package br.com.conexa.desafio.backend.service;

import br.com.conexa.desafio.backend.exception.TokenNotFoundException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import org.junit.jupiter.api.*;
import org.junit.platform.commons.util.StringUtils;
import org.opentest4j.AssertionFailedError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.sql.Date;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TokenServiceTest {

    @Autowired
    private TokenService service;

    Date expirationDate = Date.valueOf(LocalDate.now().plusDays(7));

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void shouldGenerateToken() {
        String username = "medico@email.com" + Math.random();
        assertNotNull(service.generateToken(username));
    }

    @Test
    void shouldThrowUsernameNotFoundException() {
        assertThrows(UsernameNotFoundException.class, () -> service.generateToken(""));
    }

    @Test
    void shouldSave() {
        String username = "medico@email.com" + Math.random();
        assertDoesNotThrow(() -> service.save(service.generateToken(username), username, expirationDate));
    }

    @Test
    void shouldSeeIfIsValid() {
        String username = "medico@email.com" + Math.random();
        assertTrue(service.isValid(service.generateToken(username)));
    }

    @Test
    void shouldInvalidate() {
        String username = "medico@email.com" + Math.random();
        assertDoesNotThrow(() -> service.invalidate(service.generateToken(username)));
    }

    @Test
    void shouldExtractUsernameFrom() {
        String username = "medico@email.com" + Math.random();
        assertTrue(StringUtils.isNotBlank(service.extractUsernameFrom(service.generateToken(username))));
    }

    @Test
    void shouldJWTDecodeException() {
        assertThrows(JWTDecodeException.class, () -> service.isValid(""));
    }
}