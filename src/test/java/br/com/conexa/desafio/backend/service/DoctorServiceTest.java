package br.com.conexa.desafio.backend.service;

import br.com.conexa.desafio.backend.exception.NotFoundException;
import br.com.conexa.desafio.backend.model.Doctor;
import br.com.conexa.desafio.backend.repository.SpecialtyRepository;
import org.junit.jupiter.api.*;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DoctorServiceTest {

    @Autowired
    private DoctorService service;

    @Autowired
    private SpecialtyRepository specialtyRepository;

    private Doctor doctor;

    public static final String DOCTOR_NAME = "dr. Vitor Marques";
    private static final String DOCTOR_UUID = UUID.randomUUID().toString();
    private static final String INVALID_DOCTOR_UUID = UUID.randomUUID().toString();
    private static final String DOCTOR_USERNAME = "medicovitor@email.com";
    private static final String DOCTOR_PASSWORD = new BCryptPasswordEncoder().encode("senhadomedicovitor");

    @BeforeEach
    void setUp() {
        this.doctor = Doctor.builder()
                .name(DOCTOR_NAME)
                .username(DOCTOR_USERNAME)
                .password(DOCTOR_PASSWORD)
                .specialty(specialtyRepository.findById(1L).get())
                .uuid(DOCTOR_UUID)
                .build();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @Order(1)
    void shouldSave() {
        assertNotNull(service.save(doctor));
    }

    @Test
    @Order(2)
    void shouldFindByUuid() {
        assertTrue(StringUtils.isNotBlank(service.findByUuid(DOCTOR_UUID).getUsername()));
    }

    @Test
    @Order(3)
    void shouldThrowNotFoundException() {
        assertThrows(NotFoundException.class,
                () -> service.findByUuid(INVALID_DOCTOR_UUID));
    }
}